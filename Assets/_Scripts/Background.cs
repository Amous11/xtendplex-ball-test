using UnityEngine;

public class Background : MonoBehaviour
{
    [SerializeField]
    private GameObject m_CubePrefab;
    [SerializeField]
    private int m_NumberOfCubes;

    //Creates objects by Clicking on the booleans in the Editor

#if UNITY_EDITOR
    public bool m_CreateCubes = false;
    public void OnValidate()
    {
        if (m_CreateCubes)
        {
            for (int i = 0; i < m_NumberOfCubes; i++)
            {
                Vector3 pos = new Vector3(
                    Random.Range(transform.position.x - 500, transform.position.x + 500),
                    Random.Range(transform.position.y + 10, transform.position.y + 700),
                    Random.Range(transform.position.z + 200, transform.position.z + 700));

                Instantiate(m_CubePrefab, pos, Random.rotation, transform).transform.localScale = Vector3.one * Random.Range(20f, 50f);
            }

            m_CreateCubes = false;
        }
    }
#endif
}
