using UnityEngine;

public class GenerateEnemies : MonoBehaviour
{
    [SerializeField]
    private float m_MinHeight = 3f;

    //Generate enemies in current View randomly.
    
    private void FixedUpdate()
    {
        GameObject enemy = PoolManager.Instance.GetPooledObject();
        if (enemy != null)
        {
            Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(0, Screen.width), Random.Range(Screen.height * 0.15f, Screen.height), Camera.main.nearClipPlane + 10));
            if (position.y < m_MinHeight)
                return;

            enemy.transform.position = new Vector3(position.x, position.y, 0f);
            enemy.SetActive(true);
        }
    }
}
