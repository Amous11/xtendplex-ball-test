using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    #region Singleton
    public static PoolManager Instance;

    private void Awake()
    {
        Instance = this;
    }
    #endregion

    public GameObject EnemyPrefab;
    public int ListSize = 20;
    public List<GameObject> Enemies = new List<GameObject>();

    private void Start()
    {
        GameObject temp;
        for (int i=0; i < ListSize; i++)
        {
            temp = Instantiate(EnemyPrefab);
            temp.SetActive(false);
            Enemies.Add(temp);
        }
    }

    public GameObject GetPooledObject()
    {
        for (int i=0; i < ListSize; i++)
        {
            if (!Enemies[i].activeSelf) 
            { 
                return Enemies[i]; 
            }
        }
        return null;
    }
}
