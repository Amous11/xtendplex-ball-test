using System.Collections;
using UnityEngine.Rendering;
using UnityEngine;
using Cinemachine;

public class JuiceManager : MonoBehaviour
{
    #region Singleton
    public static JuiceManager Instance;

    private void Awake()
    {
        Instance = this;
    }
    #endregion

    [SerializeField, Range(0f, 100f)]
    private float m_SlowPercentage = 50f;

    [SerializeField] private CinemachineVirtualCamera m_VCam;
    [SerializeField] private CinemachineFollowZoom m_Zoom;
    [SerializeField] private float m_ShakeTimer;
    [SerializeField] private Volume m_Volume;

    private CinemachineBasicMultiChannelPerlin m_Shake;

    private float m_FixedDeltaTime;

    private void Start()
    {
        m_Shake = m_VCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        m_FixedDeltaTime = Time.fixedDeltaTime;
        m_Volume.weight = 0f;
    }

    // Smoothly slows time on mouse click and resets on mouse up 
    public void SlowTime(bool i_Bool)
    {
        if (i_Bool)
        {
            Time.timeScale = 1f - m_SlowPercentage / 100f;
        }
        else 
        { 
            Time.timeScale = 1f;
        }

        Time.fixedDeltaTime = m_FixedDeltaTime * Time.timeScale;
    }
    
    // Zooms out on mouse click, with extra screen effects
    public void ZoomOut(float i_Width)
    {
        m_Zoom.m_Width = i_Width;
        StartCoroutine(VolumeAnim(m_Volume.weight, 0.25f));
    }

    // Toggles the extra screen effects smoothly
    private IEnumerator VolumeAnim(float i_InitialWeight, float i_Time)
    {
        for (float timer = 0f; timer < i_Time; timer += Time.deltaTime)
        {
            if (i_InitialWeight < 1f) { m_Volume.weight += timer / i_Time; }
            else { m_Volume.weight -= timer / i_Time; }

            m_Volume.weight = Mathf.Clamp(m_Volume.weight, 0f, 1f);

            yield return null;
        }
    }
    
    // Camera shakes on player collision with enemy
    public void DoShake()
    {
        StartCoroutine(ShakeAnim(m_ShakeTimer));
    }

    private IEnumerator ShakeAnim(float i_Time)
    {
        m_Shake.m_AmplitudeGain = 0.5f;
        yield return new WaitForSeconds(i_Time);
        m_Shake.m_AmplitudeGain = 0f;
    }

}
