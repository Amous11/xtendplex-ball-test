using UnityEngine;

public class Follow : MonoBehaviour
{
    [SerializeField] private Transform m_PlayerTransform;

    private void Update()
    {
        transform.Translate(Vector3.right * (m_PlayerTransform.position.x - transform.position.x));
    }
}
