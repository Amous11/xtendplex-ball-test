using System.Collections;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private Collider m_Collider;
    [SerializeField]
    private MeshRenderer m_MeshRenderer;
    [SerializeField]
    private GameObject m_Particles;

    private bool m_GotDestroyed;

    private void OnEnable()
    {
        m_MeshRenderer.enabled = true;
        m_Collider.enabled = true;
        m_GotDestroyed = false;

    }

    private void OnDisable()
    {
        m_Particles.SetActive(false);
    }

    private void PlayDestruction()
    {
        m_GotDestroyed = true;
        m_Collider.enabled = false;
        m_MeshRenderer.enabled = false;
        m_Particles.SetActive(true);

        StartCoroutine(DisableAfter(2.3f));
    }

    private IEnumerator DisableAfter(float i_Time)
    {
        yield return new WaitForSeconds(i_Time);

        gameObject.SetActive(false);
    }

    private void OnBecameVisible()
    {
        // Prevents effects from teleporting due to Pooling
        StopAllCoroutines();
    }

    private void OnBecameInvisible() // Disables enemy when offscreen
    {
        // Wait for destruction animation or destroy right away
        float time;
        if (m_GotDestroyed) { time = 2.3f; }
        else { time = 0f; }

        StartCoroutine(DisableAfter(time));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag(eTag.Player.ToString()))
        {
            PlayDestruction();
        }
    }
}
