using System.Collections;
using UnityEngine;

public class ScaleAnimation : MonoBehaviour
{
    [SerializeField]
    private float m_AnimationTime;

    private void OnEnable()
    {
        StartCoroutine(ScaleUp(m_AnimationTime));
    }

    private IEnumerator ScaleUp(float i_Time)
    {
        for (float timer = 0f; timer < i_Time; timer += Time.deltaTime)
        {
            transform.localScale = Vector3.Slerp(Vector3.zero, Vector3.one, timer / i_Time);
            yield return null;
        }
    }
}
