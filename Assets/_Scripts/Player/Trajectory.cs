using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trajectory : MonoBehaviour
{
    [SerializeField] private Player m_Player;
    [SerializeField] private LineRenderer m_Line;

    [SerializeField, Range(5, 30)]
    private int m_LineResolution;

    private void Start()
    {
        EraseLine();
        m_Line.positionCount = m_LineResolution;
    }

    public void DrawLine(Vector3 i_Direction, float i_Strength)
    {
        if (!m_Line.enabled) { m_Line.enabled = true; }

        float t;
        for (int i = 0; i < m_LineResolution; i++)
        {
            t = i * 0.1f;

            Vector3 force = i_Direction.normalized * Mathf.Clamp(i_Strength, 0f, m_Player.MaxForce);
            Vector3 currentPos = (force * t) + 0.5f * Physics.gravity * (t * t);

            m_Line.SetPosition(i, transform.position + currentPos);
        }

    }

    public void EraseLine()
    {
        m_Line.enabled = false;
    }

}
