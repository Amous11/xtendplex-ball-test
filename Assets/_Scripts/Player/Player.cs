using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Rigidbody Rigidbody;
    public float MaxForce;

    [SerializeField] private ForceMode m_ForceMode;

    public void Launch(Vector3 i_Direction, float i_Strength, bool i_Reset)
    {
        if (i_Strength == 0f) { return; }
        if (i_Reset) { Rigidbody.velocity = Vector3.zero; }

        Vector3 force = i_Direction.normalized * Mathf.Clamp(i_Strength, 0f, MaxForce);
        Rigidbody.AddForce(force, m_ForceMode);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag(eTag.Enemy.ToString()))
        {
            JuiceManager.Instance.DoShake();
            Launch(Vector3.up, Rigidbody.velocity.magnitude * 2f, false);
        }
    }
}
