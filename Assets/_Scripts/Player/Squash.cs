using UnityEngine;

public class Squash : MonoBehaviour
{
    [SerializeField]
    private Rigidbody m_Rigidbody;
    [SerializeField]
    private Transform m_Body;

    private Vector3 m_Velocity = Vector3.zero;

    private void FixedUpdate()
    {
        Debug.Log(m_Rigidbody.velocity.sqrMagnitude);
        Vector3 scale = new Vector3(
            Mathf.Clamp(1f - Mathf.Abs(m_Rigidbody.velocity.normalized.x), 0.7f, 1f),
            Mathf.Clamp(1f - Mathf.Abs(m_Rigidbody.velocity.normalized.y), 0.7f, 1f),
            1f);

        m_Body.transform.localScale = Vector3.SmoothDamp(m_Body.transform.localScale, scale, ref m_Velocity, 0.15f);
        m_Body.localRotation = Quaternion.LookRotation(m_Rigidbody.velocity.normalized, Vector3.up);
    }
}
