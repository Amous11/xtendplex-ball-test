using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class Drag : MonoBehaviour
{
    [SerializeField] private Player m_Player;
    [SerializeField] private Trajectory m_Trajectory;
    [SerializeField] private InputAction m_MouseClick;

    private Vector3 m_CurrentMousePosition;
    private Vector3 m_FirstMousePosition;
    private Vector3 m_Direction;

    private float m_DragDistance;

    private void OnEnable()
    {
        m_MouseClick.Enable();
        m_MouseClick.performed += MouseClicked;
        m_MouseClick.canceled += MouseReleased;
    }

    private void OnDisable()
    {
        m_MouseClick.performed -= MouseClicked;
        m_MouseClick.canceled -= MouseReleased;
        m_MouseClick.Disable();
    }

    private void MouseClicked(InputAction.CallbackContext context)
    {
        JuiceManager.Instance.SlowTime(true);
        JuiceManager.Instance.ZoomOut(50f);
        m_FirstMousePosition = Mouse.current.position.ReadValue();
        StartCoroutine(DragUpdate());
    }

    private void MouseReleased(InputAction.CallbackContext context)
    {
        JuiceManager.Instance.SlowTime(false);
        JuiceManager.Instance.ZoomOut(0f);
        m_Player.Launch(m_Direction, m_DragDistance / 10f, true);
        m_Trajectory.EraseLine();
    }

    private IEnumerator DragUpdate()
    { 

        while (m_MouseClick.ReadValue<float>() != 0)
        {
            m_CurrentMousePosition = Mouse.current.position.ReadValue();

            m_Direction = m_FirstMousePosition - m_CurrentMousePosition;
            m_DragDistance = Vector3.Distance(m_FirstMousePosition, m_CurrentMousePosition);

            if (m_DragDistance != 0f) { m_Trajectory.DrawLine(m_Direction, m_DragDistance / 10f); }

            yield return null;
        }
        
    }

    
}
